---
title:  Propriétés 🎯
---

#  Propriétés  (Bac 🎯)

<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Licence Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png" /></a><br />Ce cours est mis à disposition selon les termes de la <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Licence Creative Commons Attribution - Pas d'Utilisation Commerciale - Partage dans les Mêmes Conditions 4.0 International</a>.


![programme](images/programme_arbre.png){: .center}



!!! cite "Sources et crédits pour ce cours"
    Pour préparer ce cours, j'ai utilisé :

    * le [manuel NSI](https://www.nsi-terminale.fr/) chez Ellipses de *Balabonski, Conchon, Filliâtre, Nguyen*
    * le [manuel NSI](https://www.mesmanuels.fr/acces-libre/9782017189992) chez Hachette sous la direction de *Michel Beaudouin Lafon*
    * [l'article sur les arbres en NSI](https://sebhoa.gitlab.io/iremi/03_Didactique/arbres/) de Sébastien Hoarau
    * le cours de mon collègue Pierre Duclosson
    * les [cours de Gilles Lassus](https://glassus.github.io/terminale_nsi/T1_Structures_de_donnees/1.1_Listes_Piles_Files/cours/) et de  [Franck Chambon](https://ens-fr.gitlab.io/nsi2/5-Lin%C3%A9aires/1-pile-0/)


 [🔖 Synthèse de ce qu'il faut retenir pour le bac](../Synthèse/synthese_abr.md){: .md-button}


## Maximum et minimum

!!! info "Point de cours 2 : maximum ou minimum dans un arbre binaire de recherche"
    Soit un arbre binaire vérifiant la propriété d'**arbre binaire de recherche**.

    * _Minimum :_  Pour déterminer le **minimum** des éléments stockés dans les noeuds de l'arbre il faut et il suffit de partir du noeud racine et de  *toujours descendre dans le fils/sous-arbre gauche* tant que celui-ci est non vide. Le **minimum** est  alors l'élément stocké dans le premier noeud atteint dont le fils/sous-arbre gauche est vide.
    *  _Maximum :_  Pour déterminer le **maximum** des éléments stockés dans les noeuds de l'arbre il faut et il suffit de partir du noeud racine et de  *toujours descendre dans le fils/sous-arbe droit* tant que celui-ci est non vide. Le **maximum** est  alors l'élément stocké dans le premier noeud atteint dont le fils/sous-arbre droit est vide.


!!! question "Exercice 2"

    === "énoncé"
        Déterminez le maximum et le minimum des éléments stockés dans l'arbre binaire de recherche ci-dessous :

        ![alt](images/abr_iata_ex2.png)

    === "solution"
        En appliquant les algorithmes décrits dans le point de cours 2: 

        * le *minimum* des éléments stockés dans l'arbre est `'DLY'`
        * le *maximum* des éléments stockés dans l'arbre st `'ZQF'` 


## Parcours infixe

!!! info "Point de cours 3 : parcours infixe d'un arbre binaire de recherche"
    Le **parcours infixe** d'un arbre binaire vérifiant la propriété d'**arbre binaire de recherche** donne une **énumération dans l'ordre croissant** des éléments stockés dans les noeuds.


!!! question "Exercice 3"

    === "énoncé"
        Énumérez dans l'ordre du parcours infixe les éléments stockés dans les noeuds de l'arbre binaire de recherche de l'exercice 2.


    === "solution"
        En énumérant les élément dans l'ordre du parcours infixe, on obtient les éléments dans l'ordre croissant :

        ~~~
        'DLY' - 'FFO' - 'KME' - 'QLT' - 'THG' - 'TSA' - 'YXT' - 'ZQF'
        ~~~


