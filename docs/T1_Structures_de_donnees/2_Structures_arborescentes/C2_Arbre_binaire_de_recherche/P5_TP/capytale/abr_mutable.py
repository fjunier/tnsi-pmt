#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Interface d'ABR mutable avec deux classes Noeud et ABR
"""
from collections import deque


class Noeud:
    """Noeud pour arbre binaire"""
    
    def __init__(self, g, e, d):
        self.gauche = g # lien vers fils gauche g éventuellement vide (None)
        self.element = e # élément e stocké dans le noeud
        self.droit = d # lien vers fils droit d éventuellement vide (None)


class ABR:
    """Classe d'arbre binaire mutable"""
    
    def __init__(self):
        """Constructeur, self.racine point vers None si arbre vide
        ou le noeud racine"""
        self.racine = None

    def est_vide(self):
        """Teste si l'arbre est vide, renvoie un booléen"""
        return self.racine is None

    def droit(self):
        """Renvoie le sous-arbre (de type Arbre) fils droit de l'arbre 
        Provoque une erreur si arbre est vide"""
        assert not self.est_vide()
        return self.racine.droit
    
    def gauche(self):
        """Renvoie le sous-arbre (de type ABR)  gauche de l'arbre 
        Provoque une erreur si arbre est vide"""
        assert not self.est_vide()
        return self.racine.gauche
    
    def element_racine(self):
        """Renvoie l'élément stocké dans le noeud racine de l'arbre 
        Provoque une erreur si arbre est vide"""
        assert not self.est_vide()
        return self.racine.element
    
    # extension de l'interface
    
    
    def recherche(self, elt):
        """
        Renvoie True si element dans l'arbre binaire de recherche
        et False sinon        
        """
        if self.est_vide(): # cas de l'arbre vide
            return False
        elif elt < self.element_racine():
            return self.gauche().recherche(elt)
        elif elt > self.element_racine():
            return self.droit().recherche(elt)
        else:
            return True
        
    def recherche2(self, elt):
        """
        Renvoie True si element dans l'arbre binaire de recherche
        et False sinon        
        """
        if self.est_vide(): # cas de l'arbre vide
            return False
        elif elt < self.racine.element:
            return self.racine.gauche.recherche2(elt)
        elif elt > self.racine.element:
            return self.racine.droit.recherche2(elt)
        else:
            return True
    
    def ajoute(self, elt):
        """Ajoute elt dans une feuille de l'arbre binaire de recherche
        Maintient la propriété d'arbre binaire de recherche."""
        if self.est_vide():
            self.racine = Noeud(ABR(), elt, ABR())
        elif elt <= self.element_racine():
            self.gauche().ajoute(elt)
        else:
            self.droit().ajoute(elt)
    
    def ajoute2(self, elt):
        """Ajoute elt dans une feuille de l'arbre binaire de recherche
        Maintient la propriété d'arbre binaire de recherche."""
        if self.est_vide():
            self.racine = Noeud(ABR(), elt, ABR())
        elif elt < self.racine.element:
            self.racine.gauche.ajoute2(elt)
        else:
            self.racine.droit.ajoute2(elt)
    
    
    def parcours_infixe(self):
        """Renvoie une trace du parcours infixe de l'arbre binaire de recherche
        dans un tableau
        """
        rep = []
        if self.est_vide():
            return rep
        rep.extend(self.gauche().parcours_infixe())
        rep.append(self.element_racine())
        rep.extend(self.droit().parcours_infixe())
        return rep
    
    def minimum(self):
        """Renvoie le minimum de l'ensemble des éléments
        stockés dans l'arbre"""
        assert not  self.est_vide(), "arbre vide"
        if self.gauche().est_vide():
            return self.element_racine()
        else:
            return self.gauche().minimum()
    
    def maximum(self):
        """Renvoie le maximum de l'ensemble des éléments
        stockés dans l'arbre"""
        assert not  self.est_vide(), "arbre vide"
        if self.racine.droit.est_vide():
            return self.racine.element
        else:
            return self.racine.droit.maximum()        
    
    def taille(self):
        if self.est_vide():
            return 0
        return 1 + self.gauche().taille() +  self.droit().taille()
    
    def hauteur(self):
        if self.est_vide():
            return 0
        return 1 + max(self.gauche().hauteur(), self.droit().hauteur())
    
    def to_dot(self, path='binarytree.dot'):
        """Renvoie une représentation de l'arbre sous forme de chaine de caractères
        au format dot, à coller sans les délimiteurs ' et '
        dans http://dreampuf.github.io/GraphvizOnline/"""
        lignes = ["digraph {edge [dir = none];"]             
        numero_noeud = 0
        file = deque([(self, numero_noeud)])  
        pere = {(self, numero_noeud): None}
        while len(file) > 0:
            courant, numero = file.popleft()
            if not courant.est_vide():
                lignes.append(str(numero) + f'[label="{courant.racine.element}"]' + ";")
            else:
                lignes.append(str(numero) + '[shape=circle, style = invis]' + ";")
            if pere[(courant, numero)] is not None:
                lignes.append(str(pere[(courant, numero)])  + '->' +   str(numero)  + ";")
            if not courant.est_vide():
                fg = courant.gauche()
                fd = courant.droit()              
                numero_noeud += 1
                pere[(fg, numero_noeud)] = numero
                file.append((fg, numero_noeud ))
                numero_noeud += 1
                pere[(fd, numero_noeud)] = numero
                file.append((fd, numero_noeud))
        lignes.append("}")
        contenu = "".join(lignes)
        f = open(path, mode='w')
        f.write(contenu)
        f.close()
        return contenu
    
    def __str__(self):
        """Affichage joli d'un arbre binaire construit avec la classe Noeud
        Analogue à la fonction builtin str"""    

        def aux(arbre):
            """Fonction récursive auxiliaire"""
            if arbre.est_vide():
                return ['']
            lignes_sag = aux(arbre.gauche())
            lignes_sad  = aux(arbre.droit())
            decalage_horizontal = 2 + len(str(arbre.element_racine()))
            rep = str(arbre.element_racine()) + '_' * 2 + lignes_sag[0] + '\n'
            for ligne in lignes_sag[1:]:
                rep = rep + '|' +  ' ' * (decalage_horizontal - 1) + ligne + '\n'
            rep = rep + '|\n'
            rep = rep +  '|' + '_' * (decalage_horizontal - 1) + lignes_sad[0] + '\n'        
            for ligne in lignes_sad[1:]:
                rep = rep + ' ' * decalage_horizontal + ligne + '\n'
            rep = rep.rstrip()
            return rep.split('\n')

        rep = aux(self)
        return '\n'.join(rep)
    
# tests unitaires
def test_abr_mutable():
    a = ABR()
    a.racine = Noeud(ABR(), 6,  ABR())
    b = ABR()
    b.racine = Noeud(ABR(), 4, a)
    c = ABR()
    c.racine = Noeud(ABR(), 9, ABR())
    d = ABR()
    d.racine = Noeud(ABR(), 12, ABR())
    e = ABR()
    e.racine = Noeud(c, 10, d)
    f = ABR()
    f.racine = Noeud(b, 8, e)
    assert f.taille() == 6
    assert f.hauteur() == 3
    assert f.maximum() == 12
    assert f.minimum() == 4
    assert f.parcours_infixe() == [4, 6, 8, 9, 10, 12]
    print("Tests réussis")
    
if __name__ == "__main__":
    test_abr_mutable()
