class Noeud:
    """Noeud pour arbre binaire"""
    
    def __init__(self, g, e, d):
        self.gauche = g # lien vers fils gauche g éventuellement vide (None)
        self.element = e # élément e stocké dans le noeud
        self.droit = d # lien vers fils droit d éventuellement vide (None)


class ABR:
    """Classe d'arbre binaire de recherche mutable"""
    
    def __init__(self):
        """Constructeur, self.racine point vers None si arbre vide
        ou le noeud racine"""
        self.racine = None

    def est_vide(self):
        """Teste si l'arbre est vide, renvoie un booléen"""
        return self.racine is None

    def droit(self):
        """Renvoie le sous-arbre (de type Arbre) fils droit de l'arbre 
        Provoque une erreur si arbre est vide"""
        assert not self.est_vide()
        return self.racine.droit
    
    def gauche(self):
        """Renvoie le sous-arbre (de type ABR)  gauche de l'arbre 
        Provoque une erreur si arbre est vide"""
        assert not self.est_vide()
        return self.racine.gauche
    
    def element_racine(self):
        """Renvoie l'élément stocké dans le noeud racine de l'arbre 
        Provoque une erreur si arbre est vide"""
        assert not self.est_vide()
        return self.racine.element

    # extension de l'interface  
    def minimum(self):
        """Renvoie le minimum de l'ensemble des éléments
        stockés dans l'arbre"""
        assert not  self.est_vide(), "arbre vide"
        # à compléter
        
        
    def maximum(self):
        """Renvoie le maximum de l'ensemble des éléments
        stockés dans l'arbre"""
        assert not  self.est_vide(), "arbre vide"
        # à compléter  
       
    
    def taille(self):
        """Renvoie la taille de l'arbre binaire de recherche"""
        # à compléter
        
       
    
    def hauteur(self):
        """Renvoie la hauteur de l'arbre binaire de recherche"""
        # à compléter
        

    def parcours_infixe(self):
        """Renvoie une trace du parcours infixe de l'arbre binaire de recherche abr
        dans le tableau dynamique tab"""
        # à compléter
        tab = []
        if self.est_vide():
            return ...
        tab.extend(...)
        tab.append(...)
        tab.extend(...)
        return tab
        
    def compte(self, elt):
        """Renvoie le nombre d'occurrences de elt dans l'arbre
        """
        # à compléter
        
        
    
    def __str__(self):
        """Affichage joli d'un arbre binaire construit avec la classe Noeud
        Analogue à la fonction builtin str"""    

        def aux(arbre):
            """Fonction récursive auxiliaire"""
            if arbre.est_vide():
                return ['']
            lignes_sag = aux(arbre.gauche())
            lignes_sad  = aux(arbre.droit())
            decalage_horizontal = 2 + len(str(arbre.element_racine()))
            rep = str(arbre.element_racine()) + '_' * 2 + lignes_sag[0] + '\n'
            for ligne in lignes_sag[1:]:
                rep = rep + '|' +  ' ' * (decalage_horizontal - 1) + ligne + '\n'
            rep = rep + '|\n'
            rep = rep +  '|' + '_' * (decalage_horizontal - 1) + lignes_sad[0] + '\n'        
            for ligne in lignes_sad[1:]:
                rep = rep + ' ' * decalage_horizontal + ligne + '\n'
            rep = rep.rstrip()
            return rep.split('\n')

        rep = aux(self)
        return '\n'.join(rep)
    
# tests unitaires
def test_abr_mutable():
    a = ABR()
    a.racine = Noeud(ABR(), 6,  ABR())
    b = ABR()
    b.racine = Noeud(ABR(), 4, a)
    c = ABR()
    c.racine = Noeud(ABR(), 9, ABR())
    d = ABR()
    d.racine = Noeud(ABR(), 12, ABR())
    e = ABR()
    e.racine = Noeud(c, 10, d)
    f = ABR()
    f.racine = Noeud(b, 8, e)
    assert f.taille() == 6
    assert f.hauteur() == 3
    assert f.maximum() == 12
    assert f.minimum() == 4    
    assert f.parcours_infixe() == [4, 6, 8, 9, 10, 12]
    print("Tests réussis")