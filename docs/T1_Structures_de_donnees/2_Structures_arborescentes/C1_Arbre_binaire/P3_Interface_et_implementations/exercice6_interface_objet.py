class Noeud:
    """Noeud pour arbre binaire"""
    
    def __init__(self, g, e, d):
        self.gauche = g # lien vers fils gauche g éventuellement vide (None)
        self.element = e # élément e stocké dans le noeud
        self.droit = d # lien vers fils droit d éventuellement vide (None)


class AB:
    """Classe d'arbre binaire mutable"""
    
    def __init__(self):
        """Constructeur, self.racine pointe vers None si arbre vide
        ou le noeud racine"""
        self.racine = None

    def est_vide(self):
        """Teste si l'arbre est vide, renvoie un booléen"""
        return self.racine is None

    def droit(self):
        """Renvoie le sous-arbre (de type Arbre) fils droit de l'arbre 
        Provoque une erreur si arbre est vide"""
        assert not self.est_vide()
        return self.racine.droit
    
    def gauche(self):
        """Renvoie le sous-arbre (de type Arbre) fils gauche de l'arbre 
        Provoque une erreur si arbre est vide"""
        assert not self.est_vide()
        return self.racine.gauche
    
    def ajout_racine(self, element):
        """Ajoute un noeud stockant element à la racine de l'arbre
        Provoque une erreur si arbre est non vide"""
        assert self.est_vide()
        self.racine = Noeud(AB(), element, AB())
    
    def element_racine(self):
        """Renvoie l'élément stocké dans le noeud racine de l'arbre 
        Provoque une erreur si arbre est vide"""
        assert not self.est_vide()
        return self.racine.element
    
    # extension de l'interface
    
    def extreme_gauche(self):
        """Renvoie None si l'arbre est vide
        ou la valeur du noeud atteinte en descendant toujours dans le 
        sous-arbre gauche
        """
        # à compléter
        ...
        

def test():
    a = AB()
    a.ajout_racine('+')
    a.gauche().ajout_racine('*')
    a.gauche().gauche().ajout_racine(3)
    a.gauche().droit().ajout_racine('-')
    a.gauche().droit().gauche().ajout_racine(4)
    a.gauche().droit().droit().ajout_racine(5)
    a.droit().ajout_racine('*')
    a.droit().gauche().ajout_racine(5)
    a.droit().droit().ajout_racine('+')
    a.droit().droit().gauche().ajout_racine(12)
    a.droit().droit().droit().ajout_racine(7)
    assert a.extreme_gauche() == 3
    assert a.element_racine() == '+'
    assert a.gauche().element_racine() == '*'
    assert a.gauche().droit().droit().element_racine() == 5
    print("tests réussis")