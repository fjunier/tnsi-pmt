from collections import deque

class File:
    
    def __init__(self):
        """Construit une file vide"""
        self.contenu = deque([])
        
    def file_vide(self):
        """Teste si une file est vide"""
        return len(self.contenu) == 0

    def defiler(self):
        """
        Extrait l'élément en tête de file
        Coût constant, complexité en O(1)
        """
        assert not self.file_vide(), "File Vide"
        return self.contenu.popleft()
    
    def enfiler(self, elt):
        """
        Insère elt en queue de file
        Coût constant, complexité en O(1)
        """
        self.contenu.append(elt)


    # interface étendue
    def __str__(self):
        sortie = "debut : " 
        tmp = File()
        while not self.file_vide():
            elt = self.defiler()
            sortie = sortie + str(elt) + ' - '
            tmp.enfiler(elt)
        while not tmp.file_vide():
            self.enfiler(tmp.defiler())        
        sortie = sortie.rstrip(' - ') + ': fin'
        return sortie
    

def test_file():
    """
    Tests unitaires de la classe File
    """
    f = File()
    for k in range(1, 6):
        f.enfiler(k)
    for k in range(1, 6):
        assert f.defiler() == k
    assert f.file_vide()
    print("Tests réussis")

