from collections import deque

class Graphe:
    
    def __init__(self, liste_sommets):
        """
        Crée une représentation de  graphe non orienté à partir d'une liste de sommets
        """
        self.liste_sommets = liste_sommets
        self.adjacents = {sommet : [] for sommet in liste_sommets}
        
    def sommets(self):
        """
        Renvoie une liste des sommets
        """
        return self.liste_sommets
    
    def ajoute_arc(self, sommetA, sommetB):
        """
        Ajoute dans la représentation de graphe l'arc sommetA - sommetB
        """
        assert (sommetA in self.liste_sommets), "sommet A pas dans le graphe"
        assert (sommetB in self.liste_sommets), "sommet B pas dans le graphe"
        if sommetB not in self.adjacents[sommetA]:
            self.adjacents[sommetA].append(sommetB)
        if sommetA not in self.adjacents[sommetB]:
            self.adjacents[sommetB].append(sommetA)

    def voisins(self, sommet):
        """
        Renvoie une liste des voisins du sommet dans la représentation du graphe
        """
        assert sommet in self.liste_sommets, "sommet pas dans le graphe"
        return self.adjacents[sommet]

    def est_arc(self, sommetA, sommetB):
        """
        Renvoie un booléen indiquant si l'arc sommetA - sommetB appartient au graphe
        """
        assert sommetA in self.liste_sommets, "sommetA pas dans le graphe"
        return sommetB in self.adjacents[sommetA]
    

class File:
    
    def __init__(self):
        """Construit une file vide"""
        self.contenu = deque([])
        
    def file_vide(self):
        """Teste si une file est vide"""
        return len(self.contenu) == 0

    def defiler(self):
        """
        Extrait l'élément en tête de file
        Coût constant, complexité en O(1)
        """
        assert not self.file_vide(), "File Vide"
        return self.contenu.popleft()
    
    def enfiler(self, elt):
        """
        Insère elt en queue de file
        Coût constant, complexité en O(1)
        """
        self.contenu.append(elt)
    
class Pile:
    
    def __init__(self):
        """Construit une pile vide"""
        self.contenu = []
        
    def pile_vide(self):
        """Teste si une file est vide"""
        return len(self.contenu) == 0

    def depiler(self):
        """
        Extrait l'élément au sommet de la pile
        Coût constant, complexité en O(1)
        """
        assert not self.pile_vide(), "Pile Vide"
        return self.contenu.pop()
    
    def empiler(self, elt):
        """
        Insère elt au sommet de la pile
        Coût constant, complexité en O(1)
        """
        self.contenu.append(elt)


def explo_laby_dfs(debut, fin, laby):
    """Exploration avec parcours en profondeur
    du graphe non orienté laby 
    depuis le  sommet debut jusqu'au sommet fin
    Renvoie la liste trace des sommets traversés
    """
    decouvert = {s: False for s in laby.sommets()}    
    en_attente = Pile()
    en_attente.empiler(debut)
    trace = []
    # à compléter
    return trace

def explo_laby_bfs(sommet, sortie, laby):
    """Exploration avec parcours en largeur
    du graphe non orienté laby 
    depuis le  sommet debut jusqu'au sommet fin
    Renvoie la liste trace des  couples (sommet traversé, distance à debut)
    """
    distance = {s:float('inf') for s in laby.sommets()}
    decouvert = {s: False for s in laby.sommets()}    
    en_attente = File()
    decouvert[sommet] = True
    distance[sommet] = ... # à compléter
    en_attente.enfiler(sommet)
    trace = []
    while not en_attente.file_vide():
        s = en_attente.defiler()
        #  à compléter
        ...
        if s == sortie:
            break
        for v in laby.voisins(s):
            if not decouvert[v]:
                #  à compléter
                ...
    return trace

#On génère le labyrinthe
sommets = [(i, j) for i in range(6) for j in range(5)]
laby_wikipedia = Graphe(sommets)
liste_arcs = [((0,4), (1, 4)), ((1,4), (2, 4)), ((2,4), (2, 3)), ((2,3), (2, 2)),((2,3), (3, 3))
              , ((2,2), (1, 2)), ((1, 2), (1, 3)), ((1,3), (0, 3)), ((2, 2), (3, 2)),  ((3, 2), (3, 1)),
              ((3, 1), (2, 1)), ((2, 1), (1, 1)),((1, 0), (0, 0)), ((0, 0), (0, 1)),((2, 1), (2, 0)),
               ((2, 0), (3, 0)), ((3, 0), (4, 0)),((2, 4), (3, 4)), ((3, 4), (4, 4)),
                ((4, 4), (4, 3)),((4, 3), (4, 2)), ((4, 2), (5, 2)),((5, 2), (5, 1)),((5, 1), (5, 0)),
                  ((5, 2), (5, 3)), ((5, 2), (5, 3)),  ((5, 3), (5, 4))]
for (i, j) in liste_arcs:
    laby_wikipedia.ajoute_arc(i, j)

def test_laby_dfs():
    """Test unitaire pour laby_dfs"""
    attendu = [(0, 4), (1, 4), (2, 4), (3, 4), (4, 4), (4, 3), (4, 2), (5, 2), (5, 3), (5, 4)]
    assert explo_laby_dfs((0,4), (5,4), laby_wikipedia)
    print("Tests réussis")

def test_laby_bfs():
    """Test unitaire pour laby_bfs"""
    attendu = [((0, 4), 0), ((1, 4), 1), ((2, 4), 2), ((2, 3), 3), 
               ((3, 4), 3), ((2, 2), 4), ((3, 3), 4), ((4, 4), 4), 
               ((1, 2), 5), ((3, 2), 5),
               ((4, 3), 5), ((1, 3), 6), 
               ((3, 1), 6), ((4, 2), 6), ((0, 3), 7), 
               ((2, 1), 7), ((5, 2), 7), ((1, 1), 8), 
               ((2, 0), 8), ((5, 1), 8), ((5, 3), 8), ((3, 0), 9),
               ((5, 0), 9), ((5, 4), 9)]
    assert explo_laby_bfs((0,4), (5,4), laby_wikipedia) == attendu
    print("Tests réussis")