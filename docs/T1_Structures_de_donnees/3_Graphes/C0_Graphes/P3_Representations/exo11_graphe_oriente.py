class Graphe_oriente:
    
    def __init__(self, liste_sommets):
        """
        Crée une représentation de  graphe  orienté à partir d'une liste de sommets
        Représentation par dictionnaire d'adjacences
        """
        self.liste_sommets = liste_sommets
        self.adjacents = {sommet : [] for sommet in liste_sommets}
        
    def sommets(self):
        """
        Renvoie une liste des sommets
        """
        # à compléter

    def ajoute_arc(self, sommetA, sommetB):
        """
        Ajoute dans la représentation de graphe l'arc sommetA -> sommetB
        """
        assert (sommetA in self.liste_sommets), "sommet A pas dans le graphe"
        assert (sommetB in self.liste_sommets), "sommet B pas dans le graphe"
        # à compléter


    def voisins(self, sommet):
        """
        Renvoie une liste des voisins du sommet dans la représentation du graphe
        """
        assert sommet in self.liste_sommets, "sommet pas dans le graphe"
        # à compléter

    def est_arc(self, sommetA, sommetB):
        """
        Renvoie un booléen indiquant si l'arc sommetA -> sommetB appartient au graphe
        """
        assert sommetA in self.liste_sommets, "sommetA pas dans le graphe"
        # à compléter
    
    def degre_entrant(self, sommet):
        """
        Renvoie le degré entrant d'un sommet du graphe orienté
        """
        # à compléter
    
    def degre_sortant(self, sommet):
        """
        Renvoie le degré sortant d'un sommet du graphe orienté
        """
        # à compléter
    
    
def test_graphe_oriente():
    """
    Tests unitaires pour la classe Graphe_oriente
    """
    g1 = Graphe_oriente([0, 1, 2, 3, 4])
    g1.ajoute_arc(1, 2)
    g1.ajoute_arc(1, 4)
    g1.ajoute_arc(2, 3)
    g1.ajoute_arc(2, 4)
    g1.ajoute_arc(3, 4)
    g1.ajoute_arc(4, 0)
    assert g1.est_arc(1, 2) == True, "échec sur g1.est_arc(1, 2)"
    assert g1.est_arc(2, 1) == False, "échec sur g1.est_arc(2, 1)"
    assert g1.sommets() == [0, 1, 2, 3, 4], "échec sur g1.sommets()"
    assert g1.voisins(2) == [3, 4], "échec sur g1.voisins(2)"
    assert g1.voisins(1) == [2, 4], "échec sur g1.voisins(1)"
    assert g1.degre_sortant(2) == 2, "échec sur self.degre_sortant(2)"
    assert g1.degre_entrant(2) == 1, "échec sur self.degre_entrant(2)"
    print("Tests réussis")
    

    