def creer_dico():
    return dict()

def ajouter(dico, clef, valeur):
    "à compléter"
    

def valeur(dico, clef):
    assert clef in dico, "La clef n'est pas dans le dictionnaire"
    "à compléter"
    
def test_dico():
    annuaire = creer_dico()
    ajouter(annuaire, "Eric", "0642454712")
    ajouter(annuaire, "Bob", "0908141719")
    assert valeur(annuaire, "Eric") == "0642454712"
    assert valeur(annuaire, "Bob") == "0908141719"
    ajouter(annuaire, "Eva", "0908141719")
    assert valeur(annuaire, "Eva") == "0908141719"
    print("Tests réussis")