class Tableau:
    
    def __init__(self, taille):
        self.taille = taille
        self.tab = [None for _ in range(self.taille)]


    def lire_case(self, index):
        assert isinstance(index, int), "index doit être un entier"
        assert 0 <= index < self.taille, "index en dehors de la plage licite"
        # à compléter

    def modifier_case(self, index, valeur):
        assert isinstance(index, int), "index doit être un entier"
        assert 0 <= index < self.taille, "index en dehors de la plage licite"
        # à compléter
        
        
def test_tableau():
    numeros = Tableau(10)
    numeros.modifier_case(2, "0642454712")
    assert numeros.lire_case(2) == "0642454712"
    numeros.modifier_case(0, "0842454912")
    assert numeros.lire_case(0) == "0842454912"
    print("Tests réussis")