---
title: Liste 🎯
---

#  Liste (Bac 🎯)

<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Licence Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png" /></a><br />Ce cours est mis à disposition selon les termes de la <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Licence Creative Commons Attribution - Pas d'Utilisation Commerciale - Partage dans les Mêmes Conditions 4.0 International</a>.


![programme](images/programme.png){: .center}



!!! cite "Sources et crédits pour ce cours"
    Pour préparer ce cours, j'ai utilisé :

    * le [manuel NSI](https://www.nsi-terminale.fr/) chez Ellipses de *Balabonski, Conchon, Filliâtre, Nguyen*
    * le [manuel NSI](https://www.mesmanuels.fr/acces-libre/9782017189992) chez Hachette sous la direction de *Michel Beaudouin Lafon*
    * la [ressource Eduscol sur les types de données](https://eduscol.education.fr/document/10106/download)
    * le cours de mon collègue Pierre Duclosson


 [🔖 Synthèse de ce qu'il faut retenir pour le bac](){: .md-button}

## Type abstrait Liste


!!! note "Point de cours 3"
    Le **type abstrait Liste** permet de créer une séquence  d'éléments ordonnés par leur position dans la liste. 
    
    * Un liste peut être vide.
    * Si la liste est non vide, contrairement aux types abstraits tableau ou dictionnaire, un seul élément de la liste est accessible directement, c'est la **tête** de liste
    * La **queue** de liste permet d'accéder aux éléments suivants. La queue de liste est une liste donc on accède au deuxième élément de la liste (s'il existe) en prenant la tête de la queue de liste. De même pour accéder au troisième élément on prend la tête de la queue de la queue de liste ...  

    L'accès aux éléments successifs de la liste n'est donc pas direct mais nécessite une répétition d'opérations identiques, appelée **parcours de liste**. On dit qu'une liste  est une **structure linéaire**.

    Une interface minimale du **type abstrait Liste**  est la suivante :

    |Opération|Signature|Description|
    |---|---|---|
    |creer_liste|creer_liste()|Renvoie une liste vide|
    |liste_vide|liste_vide(lis)|Renvoie un booléen indiquant si la liste `lis` est vide|
    |inserer|inserer(lis, elt)|Insère `elt` en tête de la  liste `lis`, renvoie une nouvelle liste ou modifie la liste en place|
    |tete|tete(lis)|Renvoie l'élément en tête de liste|
    |queue|queue(lis)|Renvoie la liste privée de l'élément en tête liste|

    !!! warning "Liste homogène"
        *Dans tout le chapitre on se restreint à manipuler uniquement des listes dont tous les éléments sont de même type.*

!!! question "Exercice 3"

    [💻 Saisir ses réponses sur Capytale](https://capytale2.ac-paris.fr/web/c/ec74-1763635){: .md-button}

    === "énoncé"
        Complétez l'implémentation du **type abstrait Liste** avec un tableau dynamique Python pour stocker les éléments.

        {{ IDE('exercice3_liste') }}

    === "solution"

        ~~~python
        def creer_liste():
            return []

        def liste_vide(lis):
            return len(lis) == 0

        def inserer(lis, elt):
            lis.append(elt)

        def tete(lis):
            return lis[len(lis) - 1]

        def queue(lis):
            return [lis[k] for k in range(0, len(lis) - 1)]

        def test():
            lis1 = creer_liste()
            inserer(lis1, 10)
            inserer(lis1, 9)
            assert tete(lis) == 9
            lis2 = queue(lis1)
            assert tete(lis2) == 10
            print("Tests réussis")
        ~~~


## Parcours


!!! tip  "Méthode 2"

    Le parcours d'une **liste** est une répétition d'opérations `tete` pour lire l'élément accessible en tête de liste et  `queue` pour passer à l'élément suivant.

    Cette répétition peut s'effectuer de façon *itérative* avec une boucle ou *récursive*.

    !!! example "Exemple 2"

        On peut écrire une fonction déterminant la longueur d'une liste de façon itérative ou récursive. Retenez bien ce modèle, qu'on peut décliner pour tous les parcours de liste.

        !!! info "Parcours de liste itératif"
            ~~~python
            def longueur(lis):
                lon = 0
                courant = lis  # élément en tête
                while not liste_vide(courant):
                    # si besoin traitement sur tete(courant)
                    lon = lon + 1 
                    courant = queue(courant)
                return lon
            ~~~

        !!! info "Parcours de liste récursif"
            ~~~python
            def longueur_rec(lis):
                if liste_vide(lis):
                    return 0
                return 1 + longueur_rec(queue(lis))
            ~~~

!!! question "Exercice 4"

    [💻 Saisir ses réponses sur Capytale](https://capytale2.ac-paris.fr/web/c/ec74-1763635){: .md-button}


    !!! success "Question 1"

        === "énoncé"
            Écrivez une fonction qui prend en paramètre une liste de nombres et qui renvoie leur somme. Donnez une version itérative et une version récursive.

        

        === "solution"

            Version itérative :

            ~~~python
            def somme(lis):
                s = 0
                courant = lis  # élément en tête
                while not liste_vide(courant):
                    # si besoin traitement sur tete(courant)
                    s = s + tete(courant)
                    courant = queue(courant)
                return s 
            ~~~

            Version récursive :

            ~~~python
            def somme_rec(lis):
                if liste_vide(lis):
                    return 0
                return tete(lis) + somme_rec(queue(lis))
            ~~~

    !!! success "Question 2"
    
        === "énoncé"
            Écrivez une fonction `index` qui prend en paramètre une liste de nombres et un nombre et qui renvoie l'indice de la première occurrence du nombre  dans la liste si  il s'y trouve et `None` sinon. On numérotera les positions à partir de 0 pour la tête de liste.

            Donnez une version itérative et une version récursive.

        

        === "solution"

            Version itérative :

            ~~~python
            def index(lis, v):
                s = 0
                courant = lis  # élément en tête
                pos = 0
                while (not liste_vide(courant)) and (tete(courant) != v):
                    courant = queue(courant)
                    pos = pos + 1
                if liste_vide(courant):
                    return None
                return pos
            ~~~

            Version récursive avec accumulateurs :

            ~~~python
            def index_rec(lis, v, pos):
                if liste_vide(lis):
                    return None
                if tete(lis) == v:
                    return pos
                return index_rec(queue(lis), v, pos + 1)
            ~~~

    !!! success "Question 3"

        === "énoncé"
            Peut-on faire une recherche dichotomique  dans une liste dont les éléments sont triés dans l'ordre croissant avec la même efficacité que dans un tableau ?

        === "solution"
            Pas de façon efficace  car on ne peut pas accéder directement à tous les  éléments comme dans un tableau, seul l'élément en tête de liste est accessible directement. Pour accéder à l'élément en position $k$ il faut parcourir la liste de l'élément de tête jusqu'à celui en position $k$.


