def creer_liste():
    return None

def liste_vide(lis):
    return lis is None

def inserer(lis, elt):
    """Insère elt en  tête de lis et renvoie la  liste créée"""
    # à compléter
    ...


def tete(lis):
    """Renvoie l'élément en tête de la liste lis"""
    # à compléter
    ...

def queue(lis):
    """Renvoie une liste qui est la queue de lis"""
    # à compléter
    ...

def test():
    lis = creer_liste()
    assert liste_vide(lis)
    for k in range(0, 3):
        lis = inserer(lis, k)
    assert lis == (2, (1, (0, None)))
    print("Tests réussis")