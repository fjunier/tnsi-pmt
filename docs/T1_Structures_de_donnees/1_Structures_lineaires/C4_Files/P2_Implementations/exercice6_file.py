from collections import deque

class File4:
    
    def __init__(self):
        self.contenu = deque([])
        
    def file_vide(self):
        return len(self.contenu) == 0

    def defiler(self):
        assert not self.file_vide(), "File Vide"
        # à compléter
        ...
        
      
    def enfiler(self, elt):
        # à compléter
        ...
    
    # interface étendue
    def __str__(self):
        sortie = "debut : " 
        tmp = File4()
        while not self.file_vide():
            elt = self.defiler()
            sortie = sortie + str(elt) + ' - '
            tmp.enfiler(elt)
        while not tmp.file_vide():
            self.enfiler(tmp.defiler())        
        sortie = sortie.rstrip(' - ') + ': fin'
        return sortie
    
def test_file4():
    f = File4()
    for k in range(1, 6):
        f.enfiler(k)
    print(f)
    for k in range(1, 6):
        assert f.defiler() == k
    assert f.file_vide()
    print("Tests réussis")
    