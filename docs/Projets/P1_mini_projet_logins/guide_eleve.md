---
title:  Mini Projet guidé, logins réseaux
---


# Problème à résoudre

> __Règle 1 :__  Il faut d'abord bien comprendre le problème, ses entrées et les  sorties attendues.

Vous disposez  d'un fichier [prenom_nom.csv](https://fr.wikipedia.org/wiki/Comma-separated_values) de mille lignes ou plus, listant les utilisateurs d'un réseau informatique, dont chaque ligne est  au format `numero, nom, prenom` :

~~~python
1,Timothée,Gaillard
2,Zacharie,Lefort
3,Joseph,Lambert
~~~

Vous devez générer pour chaque utilisateur un __login__ `prenom.nom` en minuscules, sans accents, ni cédilles et un __mot de passe__ aléatoire respectant la politique suivante :
* constitué de dix symboles choisis parmi les 4 classes suivantes : caractères alphabétiques minuscules, caractères majuscules, chiffres  ou l'un des caractères spéciaux '?!$#'.
* au moins un caractère de chaque classe

Ces identifiants de connexion devront être stockés (en clair certes ...) dans un fichier [CSV](https://fr.wikipedia.org/wiki/Comma-separated_values) dont chaque ligne sera  au format `numero, nom, prenom,login,mdp` :

~~~python
1,Timothée,Gaillard,timothee.gaillard,AToKlBso4M
2,Zacharie,Lefort,zacharie.lefort,4E7hFZgXNt
3,Joseph,Lambert,joseph.lambert,G6PtmimT0J
~~~


# Cahier des charges

> __Règle 2  :__  On commence par réfléchir avec papier et crayon pour déterminer les étapes à réaliser et isoler les tâches élémentaires dans des fonctions.


## Question 1

Pour déterminer les étapes  du __cahier des charges__, complétez la cart heuristique en lien : <https://ladigitale.dev/digimindmap/#/m/64f60ee50975c> 

En *programmation modulaire*, on représente chaque tâche élémentaire par une fonction. Déterminez une  liste de fonctions qu'on pourrait écrire pour répondre  au __cahier des charges__.


## Question 2

Précisez pour chaque fonction ses _paramètres_ avec leurs types et les valeurs renvoyées avec leurs types. Une fonction peut ne prendre aucun paramètre en entrée et ne renvoyer aucune valeur en sortie.

Un exemple :

~~~python
def generer_login(prenom, nom):
    """
    Génère un login de type prenom.nom
    à partir des valeurs de  prenom et de nom convertis en minuscules
    """
~~~

Vous travaillerez en binôme avec le pad collaboratif [Digipad](https://ladigitale.dev/blog/digipad-des-murs-multimedias-pour-vos-activites-collaboratives)

# Algorithmes et structures de données

> __Règle 3  :__  Une fois qu'on a déterminé ce qu'on souhaiterait réaliser, on se pose la question du comment : de quelle façon (algorithmes) et avec  quels outils : structures ou styles de programmation (boucles, conditions, programmation objet ou fonctionnelle) et structures de données (entier, chaîne de caractères, tableau, dictionnaire, arbre ...)


## Question 3


Dans ce projet, de quels types d'algorithmes pourriez-vous avoir besoin ?

* _balayage séquentiel_ : parcourir une structure de données élément par élément
* _recherche dichotomique_ : diviser un ensemble ordonné en deux et réduire la recherche à la première ou la seconde moitié comme dans le _Juste prix_
* _de tri_ : ordonner des information selon un certain critère
* _glouton_ : pn construit une solution à un problème de recherche d'optimum global en effectuant à chaque étape le choix optimal à ce moment.

## Question 4


Parmi les types de données Python suivants, quels sont ceux qui pourraient être utiles dans ce projet ? Si oui pour quelle(s) fonction(s) ?

|Type|Nom Python|Exemple de valeur|
|:---:|:---:|:---:|
|entier|`int`|`4`|
|flottant|`float`|`3.14`|
|booléen|`bool`|`True` ou `False`|
|chaîne de caractères|`str`|`'edgar'`|
|tableau/liste|`list`|`['8', 'edgar', 'quinet']`|
|dictionnaire|`dict`|`{'nom': 'quinet', 'prenom': 'edgar'}`|

# Développement et écriture du code

>  __Règle 4  :__  Après avoir découpé la réalisation du projet en fonctions et choisi pour chacune un ou des types de structures de données et un algorithme pour les traiter, il reste à les implémenter dans le langage de programmation choisi, Python pour vous. Si on sait prédire la sortie que doit générer une fonction pour un certaine entrée, on peut construire un **jeu de tests** afin de vérifier qu'elle fonctionne comme attendu sur quelques cas bien choisis. Cela ne prouvera cependant pas qu'elle est correcte.


## Question 5


Récupérez le lien avec l'archive contenant [le matériel](materiel.zip) puis complétez les fonctions définies dans le **cahier des charges**. Le squelette de  code  contient au moins une fonction de test.  

## Question 6

En pratique, on a besoin de construire des fichiers d'identifiants de connexion régulièrement. Il serait donc d'étendre notre programme avec une [Interface Homme Machine](https://fr.wikipedia.org/wiki/Interactions_homme-machine). Dans le squelette de code, complétez la fonction d''interface textuelle puis celle d'interface graphique. Pour cette dernière vous utiliserez le module `nsi_ui` fourni dans [le matériel](materiel.zip). Une documentation est disponible page 52 du [manuel Déclic de Première NSI](https://mesmanuels.fr/acces-libre/3813624).


