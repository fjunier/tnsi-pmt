---
title: Projet seam carving
---

<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Licence Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png" /></a><br />Ce cours est mis à disposition selon les termes de la <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Licence Creative Commons Attribution - Pas d'Utilisation Commerciale - Partage dans les Mêmes Conditions 4.0 International</a>.

## Présentation

On commence le projet en classe : exercices 1 à 5, puis les élèves se répartissent en quatre groupes pour traiter les séparément les exercices 6, 7, 8 et 9. Chaque groupe rend le carnet Capytale avec son code au professeur et préparer une intervention orale de 10 minutes maximum pour le groupe où les élèves du groupe présenteront leur algorithme aux autres. C'est la performance de chaque élève lors de la présentation orale qui sera notée.

## Projet

*  [💻 Projet seam carving sur Capytale](https://capytale2.ac-paris.fr/web/c/13d4-2022906){: .md-button}
*  [💻 Projet seam carving correction sur Capytale](https://capytale2.ac-paris.fr/web/c/66e4-2310416){: .md-button}
