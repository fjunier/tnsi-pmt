---
title: Projet Pacman
---

<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Licence Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png" /></a><br />Ce cours est mis à disposition selon les termes de la <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Licence Creative Commons Attribution - Pas d'Utilisation Commerciale - Partage dans les Mêmes Conditions 4.0 International</a>.


## Présentation

Afin de préparer la [Nuit du Code](https://www.nuitducode.net/), vous allez compléter le squelette de code d'un jeu de [Pacman]() basique.

!!! note "Cahier des charges"
    
    * un seul labyrinthe dans un écran de $128 \times 128$ pixels recouvert par une grille de $16 \times 16$ tuiles de $8 \times 8$ pixels
    * les objets (pacman et fantômes) ne peuvent se déplacer que dans 4 directions : *gauche, droite, haut, bas*
    * un *pacman* qu'on peut diriger avec les flèche du pavé directionnel
    * trois *fantômes* : 
        * les fantômes *vert* et  *violet* se déplacent aléatoirement sauf lorqu'ils peuvent voir pacman (dans la même ligne ou même colonne qu'eux) dans ce cas ils se dirigent vers pacman
        * le fantôme *marron* se dirige toujours vers pacman en se déplçant  toujours sur  la tuile adjacente qui se trouve sur un *plus court chemin* vers pacman ; ce plus *plus court chemin* est déterminé à l'aide d'un [algorithme de parcours en largeur]() du graphe dont les sommets sont les tuiles libres  (carrés de $8 \times 8$ pixels) du labyrinthe et les arêtes relient les tuiles adjacentes.
    * un compteur de *pacgommes* mangées par pacman et un compteur de vies (trois par défaut avec perte d'une vide par collision avec un fantôme)
    * jeu réalisé avec le module [pyxel](https://github.com/kitao/pyxel)


## Projet

*  [💻 Projet Pacman  Capytale](https://capytale2.ac-paris.fr/web/c/2217-3518076){: .md-button}
*   [Archive avec squelette de code et fichier e ressources](./materiel_pacman.zip)
 

## Exemple


![alt](./pacman.gif){: .center}
