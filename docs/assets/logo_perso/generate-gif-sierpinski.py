##Création des images PNG

# imports des modules pygame
import pygame
from pygame.locals import *


def sierpinski(left, top, width, height):

    # avec memoization des motifs c'est beaucoup, beaucoup plus rapide
    motifs = {}

    def sierpinski2(left, top, width, height):
        if (width, height) in motifs:
            motif_surf = motifs[(width, height)]
        elif width == 1 or height == 1:
            motif_surf = pygame.surface.Surface((width, height))
            motif_surf.fill((255, 255, 255))
            motifs[(width, height)] = motif_surf
        else:
            motif_surf = pygame.surface.Surface((width, height))
            dw, dh = width // 3, height // 3
            centre_surf = pygame.surface.Surface((dw, dh))
            centre_surf.fill((64, 81, 181))
            motif_surf.blit(centre_surf, (dw, dh))
            for k in range(3):
                for j in range(3):
                    if (k, j) != (1, 1):
                        motif_surf.blit(
                            sierpinski2(k * dw, j * dh, dw, dh), (k * dw, j * dh)
                        )
            motifs[(width, height)] = motif_surf
        ecran.blit(motif_surf, (left, top))
        x, y = ecran.get_rect().center
        x, y = x - L // 6, y - L // 12
        pygame.display.flip()
        pygame.image.save(ecran, "carpette-%s.png" % (width))
        return motif_surf

    sierpinski2(left, top, width, height)
    return motifs


# initialisation des modules
pygame.init()

L = 3**6  # 3**9 provoque un dépassement de capacité mémoire

chrono = pygame.time.Clock()

debut = chrono.tick()

ecran = pygame.display.set_mode((L, L))
ecran.fill((64, 81, 181))
font = pygame.font.Font(None, 40)  # création d’un objet fonte

# ecran_rect = ecran.get_rect()
#
# surface_font = font.render('Bonne Année 2020', 1, (255, 0, 0))
# font_rect = surface_font.get_rect()
# font_rect.center = ecran_rect.center #centrage du texte
motifs = sierpinski(0, 0, L, L)


fin = chrono.tick()

print("Temps : ", (fin - debut) / 1000)
pygame.quit()


##Création du gif

import glob
import moviepy.editor as mpy

gif_name = "sierpingif"
fps = 2
file_list = glob.glob("*.png")  # Get all the pngs in the current directory
list.sort(
    file_list, key=lambda x: int(x.split("-")[1].split(".png")[0])
)  # Sort the images by #, this may need to be tweaked for your use case
clip = mpy.ImageSequenceClip(file_list, fps=fps)
clip.write_gif("{}.gif".format(gif_name), fps=fps)
