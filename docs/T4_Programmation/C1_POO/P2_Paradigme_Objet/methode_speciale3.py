class Point5:
    
    def __init__(self, x, y):
        self.x = x
        self.y = y
        
    def __repr__(self):
        return f"Point5({self.x}, {self.y})"
        
p2 = Point5(10, 3)
p3 = eval(repr(p2))
print(p3)