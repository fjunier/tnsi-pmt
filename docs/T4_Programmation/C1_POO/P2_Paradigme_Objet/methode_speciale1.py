class Point3:
    
    def __init__(self, x, y):
        self.x = x
        self.y = y
        
    def égalité(self, autre):
        return self.x == autre.x  and self.y == autre.y
        
print("Classe Point3 sans méthode spéciale __eq__")
p1 = Point3(10, 3)
p2 = Point3(10, 3)
print("p1 == p2 : ", p1 == p2)
print("p1.égalité(p2)) : ", p1.égalité(p2))



print("Classe Point4 avec méthode spéciale __eq__")


class Point4:
    
    def __init__(self, x, y):
        self.x = x
        self.y = y
        
    def __eq__(self, autre):
        return self.x == autre.x  and self.y == autre.y
        
        
p3 = Point4(10, 3)
p4 = Point4(10, 3)
print("p3 == p4 : ", p3 == p4)
