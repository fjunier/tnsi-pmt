def addition(t1, t2):
    h1, m1, s1 = t1
    h2, m2, s2 = t2
    s3 = (s1 + s2) % 60
    m3 = (m1 + m2 + (s1 + s2) // 60) % 60
    h3 = h1 + h2 + (m1 + m2 + (s1 + s2) // 60) // 60
    return (h3, m3, s3)

t1 = (1, 59, 45) # 1 heure 59 minutes 45 secondes
t2 = (2, 0, 20)  # 2 heures 0 minutes 20 secondes
t3 = addition(t1 ,t2)
print("Temps t1 : ", t1)
print("Temps t2 : ", t2)
print("Addition de t1 et t2 : ", t3)