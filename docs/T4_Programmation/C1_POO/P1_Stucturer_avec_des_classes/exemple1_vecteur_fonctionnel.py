def addition(v1, v2):
    x1 = v1[0]
    y1 = v1[1]
    x2 = v2[0]
    y2 = v2[1]
    return (x1 + x2, y1 + y2)

def mult_scal(v, k):
    x = v[0]
    y = v[1]
    return (k * x, k * y)

w1 = (10, -4)  # définition d'un premier vecteur
w2 = (-3, 6)  # définition d'un second vecteur
w3 = addition(w1, w2) # addition de deux vecteurs
w4 = mult_scal(w1, 3) # multiplication d'un vecteur par un scalaire
print("Vecteur w1 : ", w1)
print("Vecteur w2 : ", w2)
print("Addition de w1 et w2 : ", w3)
print("Multiplication de w1 par un scalaire : ", w4)