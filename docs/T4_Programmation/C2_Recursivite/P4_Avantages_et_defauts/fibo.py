import time

def fibo_rec(n):
    if n <= 1:
        return n
    else:
        return fibo_rec(n - 1) + fibo_rec(n - 2) 

tp = 1
for n in range(5, 11):
    debut = time.time()
    fn = fibo_rec(n)
    t = time.time() - debut
    print("n=", n, " f(n)=", fn, " Temps=", time.time() - debut, " Ratio=", t/tp)
    tp = t