---
title: Commandes Linux via Python
author: Frédéric Junier, Franck Chambon
tags:
    - Bash
---

# Commandes Linux via Python

> Auteurs : Frédéric Junier, Franck Chambon
> D'après 2022, Asie, J2, Ex. 1

L'entreprise capNSI gère les contrats de ses clients en créant pour chacun d'eux un sous-répertoire dans le répertoire `Contrats` sur leur ordinateur central. Le système d'exploitation de cet ordinateur est Linux. Quelques commandes de bases pour ce système d'exploitation sont rappelées ci-dessous.

!!! info "Extrait des commandes de base Linux"

    | Commande | Action |
    |----------|--------|
    | `ls`     | Afficher le contenu d'un répertoire (ex. `ls Musique`) |
    | `cd`     | Changer de répertoire de travail (ex. `cd Documents`)|
    | `cp`     | Créer une copie d'un fichier (ex. `cp fichier_1.py fichier_2.py`) |
    | `mv`     | Déplacer ou renommer un fichier ou un répertoire (ex. `mv fichier.txt répertoire`) |
    | `rm`     | Effacer un fichier ou un répertoire vide (ex. `rm mon_fichier.mp3`) |
    | `mkdir`  | Créer un répertoire vide (ex. `mkdir nouveau`) |
    | `cat`    | Afficher le contenu d'un ou plusieurs fichiers |
    | `chmod`  | Modifier les permissions d'un fichier ou d'un répertoire |


    Pour `chmod` avec un fichier, on peut l'utiliser (entre autres façons) :

    `chmod droits_propriétaire droits_groupe droits_autres nom_fichier`

    Où `droits_propriétaire`, `droits_groupe` et `droits_autres` indiquent les différents droits applicables et se construisent avec :

    - `+` ajouter
    - `-` supprimer
    - `r` *read* pour lire
    - `w` *write* pour écrire
    - `x` *execute* pour exécuter

    Exemple `chmod u+x fichier.txt`

Dans la console représentée sur la figure ci-dessous, on peut visualiser les répertoires à la racine de l'ordinateur central avec l'instruction `ls`.

```console
gestion@capNSI-ordinateur_central:~$ ls
Bureau  Documents   Modèles Public  Téléchargements
Contrats    Images  Musique Vidéos
```

!!! danger "Session Bash"
    <pre><font color="#8AE234"><b>gestion@capNSI-ordinateur_central</b></font>:<font color="#729FCF"><b>~</b></font>$ ls
    <font color="#729FCF"><b>Bureau  Documents   Modèles Public  Téléchargements
    Contrats    Images  Musique Vidéos</b></font>  son.mp3  sujet.pdf  script.py
    </pre>


1. Question 1.

    a. Donner le nom de l'utilisateur et le nom de l'ordinateur correspondant à la sortie d'écran précédente.
    b. Écrire les instructions permettant d'afficher la liste des répertoires clients du répertoire `Contrats` en partant de la situation ci-dessous :

    ```console
    gestion@capNSI-ordinateur_central:~$
    ```

    ??? success "Réponse"

        Le nom de l'utilisateur est `gestion`.
        Le nom de l'ordinateur  est `capNSI-ordinateur_central`.

        Pour afficher la listes des répertoires clients du répertoire `Contrats` en partant du répertoire personnel représenté par le raccourci `~`, on peut écrire :

        ```console
        gestion@capNSI-ordinateur_central:~$ cd Contrats
        gestion@capNSI-ordinateur_central:~/Contrats$ ls
        ```

        On peut aussi écrire plus simplement :
        
        ```console
        gestion@capNSI-ordinateur_central:~$ ls Contrats
        ```

    Après une campagne de démarchage, l'entreprise a gagné un nouveau client, monsieur Alan Turing. Elle souhaite lui créer un sous-répertoire nommé `TURING_Alan` dans le répertoire `Contrats`. De plus, elle souhaite attribuer tous les droits à l'utilisateur et au groupe et seulement la permission en lecture pour tous les autres utilisateurs. La commande `chmod` permet de le faire.

2. Question 2.

    a. Écrire les instructions permettant de créer le sous-répertoire `TURING_Alan` à partir du répertoire racine.

    ??? success "Réponse"

        Pour créer le répertoire `TURING_Alan` depuis le répertoire personnel de l'utilisateur `gestion`, on peut écrire :

        
        ```console
        gestion@capNSI-ordinateur_central:~$ mkdir Contrats/TURING_Alan
        ```


    b. Écrire l'instruction permettant d'attribuer les bons droits au sous-répertoire `TURING_Alan`.

    ??? success "Réponse"

        Pour affecter les bons droits au répertoire de chemin relatif `Contrats/TURING_Alan`, on peut écrire  :

        ```console
        gestion@capNSI-ordinateur_central:~$ chmod ug=rwx,o=r Contrats/TURING_Alan
        ```

        ou encore en notation octale (4 : lecture, 2: écriture, 1:exécution) :

        ```console
        gestion@capNSI-ordinateur_central:~$ chmod 774 Contrats/TURING_Alan
        ```


        Le chiffre des centaines 7 = 4 + 2 + 1 dénote les droits `rwx` pour le profil *propriétaire*.
        Le chiffre des dizaines 7 = 4 + 2 + 1 dénote les droits `rwx` pour le profil  *groupe*.
        Le chiffre des unités 4 = 4 + 0 + 0 dénote les droits `r--` pour le profil *autres*.

        L'exemple de syntaxe de `chmod` fourni dans l'énoncé semble incorrect, en tout cas pour la version [bash](https://fr.wikipedia.org/wiki/Bourne-Again_shell) de la ligne de commande. Voici quelques exemples de syntaxe fournis par la commande [tldr](https://tldr.sh/).

        ```console
        (py39) fjunier@fjunier:~$ tldr chmod
        chmod
        Change the access permissions of a file or directory.More information: https://www.gnu.org/software/coreutils/chmod.

        - Give the [u]ser who owns a file the right to e[x]ecute it:
        chmod u+x {{file}}

        - Give the [u]ser rights to [r]ead and [w]rite to a file/directory:
        chmod u+rw {{file_or_directory}}

        - Remove e[x]ecutable rights from the [g]roup:
        chmod g-x {{file}}

        - Give [a]ll users rights to [r]ead and e[x]ecute:
        chmod a+rx {{file}}

        - Give [o]thers (not in the file owner's group) the same rights as the [g]roup:
        chmod o=g {{file}}

        - Remove all rights from [o]thers:
        chmod o= {{file}}

        - Change permissions recursively giving [g]roup and [o]thers the ability to [w]rite:
        chmod -R g+w,o+w {{directory}}
        ```

    En Python, le module `os` permet d'interagir avec le système d'exploitation. Il permet de gérer l'arborescence des fichiers, des répertoires, de fournir des informations sur le système d'exploitation. Par exemple, le code de la page suivante, exécuté dans la console, permet de créer le sous-répertoire `TURING_Alan` précédent :

    ```pycon
    >>> import os
    >>> os.mkdir("Contrats/TURING_Alan")
    >>> os.chmod("Contrats/TURING_Alan", 774)
    ```

    L'entreprise dispose d'un tableau de nouveaux clients :

    ```python
    tableau_clients = [
        ('LOVELACE', 'Ada'),
        ('BOOLE', 'George'),
        ('VONNEUMANN', 'John'),
        ('SHANNON', 'Claude'),
        ('KNUTH', 'Donald'),
    ]
    ```

    Elle souhaite automatiser le formatage des tableaux des nouveaux clients. Elle souhaite également automatiser la création et l'attribution des droits des répertoires portant les noms des nouveaux clients.

3. Écrire une fonction `formatage(tableau)` qui prend en paramètre un tableau de
tuples `(nom, prenom)` des nouveaux clients et renvoie un tableau de chaines de caractères. Par exemple, `formatage(tableau_clients)` renvoie :

    ```python
    ['LOVELACE_Ada', 'BOOLE_George', 'VONNEUMANN_John','SHANNON_Claude', 'KNUTH_Donald']
    ```

    ??? success "Réponse"

        ```python
        def formatage(tableau):
            sortie = []
            for (nom, prenom) in tableau:
                sortie.append(nom + '_' + prenom)
            return sortie
        ```

        Autre version en compréhension :

        ```python
        def formatage(tableau):
            return [nom + '_' + prenom for (nom, prenom) in tableau]
        ```

        Ou encore sans déballage de tuple :

        ```python
        def formatage(tableau):
            return [v[0] + '_' + v[1] for v in tableau]
        ```

        Ou encore en variante fonctionnelle :

        ```python
        def formatage(tableau):
            return ['_'.join(v) for v in tableau]
        ```



4. Écrire une fonction `creation_répertoires` qui prend en paramètre un
`tableau` de chaines de caractères et qui crée et modifie les droits des
répertoires au nom de ces chaines de caractères avec les mêmes droits que le
sous-répertoire `TURING_Alan`.

    ??? success "Réponse"

        ```python
        def creation_repertoires(tableau):
            for nom_repertoire in tableau:
                repertoire = "Contrats/" + nom_repertoire
                os.mkdir(repertoire)
                os.chmod(repertoire, 774)
        ```

