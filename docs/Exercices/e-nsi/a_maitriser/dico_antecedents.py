#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb  2 21:31:08 2023

@author: fjunier
"""
def antecedents(dico):
    ante = dict()
    for clef in dico:
        if dico[clef] in ante:
            ante[dico[clef]].append(clef)
        else:
            ante[dico[clef]] = [clef]
    return ante
            
        



# tests

def trier(d): return {k: list(sorted(d[k])) for k in d}

assert trier(antecedents({'a': 5, 'b': 7})) == {5: ['a'], 7: ['b']}, "exemple 1"
assert trier(antecedents({'a': 5, 'b': 7, 'c': 5})) == {5: ['a', 'c'], 7: ['b']}, "exemple 2"
assert trier(antecedents({"Paris": "P", "Lyon": "L", "Nantes": "N", "Lille": "L"})) == \
                 