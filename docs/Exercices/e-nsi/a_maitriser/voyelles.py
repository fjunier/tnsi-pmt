#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 25 15:15:42 2023

@author: fjunier
"""
VOYELLES = ['a', 'e', 'i', 'o', 'u', 'y']

def dentiste(texte):
    res = ''
    for c in texte:
        if c in VOYELLES:
            res = res + c
    return res


# tests

assert dentiste("j'ai mal") == 'aia'
assert dentiste("il fait chaud") == 'iaiau'
assert dentiste("") == ''