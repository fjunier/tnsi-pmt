def indice(minuscule):
    return ord(minuscule) - ord("a")


def est_pangramme(phrase):
    present = [0 for i in range(26)]
    n = 0
    for c in phrase:
        i = indice(c)
        if (0 <= i < 26) and (present[i] == 0):
            present[i] = 1
            n = n + 1
    return n == 26
    

def est_pangramme(phrase):
    present = dict()
    for c in phrase:
        if ("a" <= c <= "z"):
            present[c] = True
    return len(present) == 26
    


# Tests
assert est_pangramme("portez ce vieux whisky au juge blond qui fume !") == True
assert est_pangramme("portez un vieux whisky au juge blond qui fume !") == False
assert est_pangramme("jugez que ce texte renferme l'alphabet, dix voyelles, k et w") == True
assert est_pangramme("jugez que ce texte renferme l'alphabet, dix voyelles et w") == False
