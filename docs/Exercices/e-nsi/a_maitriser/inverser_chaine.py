#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb  2 21:24:49 2023

@author: fjunier
"""
def renverser(mot):
    inv = ''
    for c in mot:
        inv = c + inv
    return inv


# Tests
renverser('informatique') == 'euqitamrofni'
renverser('nsi') == 'isn'
renverser('') == ''
