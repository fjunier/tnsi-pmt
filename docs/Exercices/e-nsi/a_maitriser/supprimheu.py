#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb  1 21:56:37 2023

@author: fjunier
"""
def supprimheu(mots):
    rep = ''
    for m in mots:
        if m != "heu":
            if rep != '':
                rep =   rep + ' ' + m
            else:
                rep =   rep +  m
    return rep




# tests

assert supprimheu(["je", "heu", "vais", "coder", "heu", "la",
 "fonction", "supprimheu"]) == 'je vais coder la fonction supprimheu'

assert supprimheu(["c", "est", "facile"]) == 'c est facile'
