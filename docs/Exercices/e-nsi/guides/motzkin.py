motzkin_mem = [1]

def motzkin(n):
    if n >= len(motzkin_mem):
        resultat = motzkin(n - 1)
        for i in range(0, n - 1):
            resultat += motzkin(i) * motzkin(n - 2 - i)
        # motzkin_mem est ici de longueur n
        motzkin_mem.append(resultat)
        # et là de longueur n + 1
    return motzkin_mem[n]


# tests

assert motzkin(4) == 9
assert motzkin(5) == 21
