def fibonacci(n):
    f = [0, 1]
    for k in range(1, n + 1):
        tmp = f[0]
        f[0] = f[1]
        f[1] = tmp + f[1]
    return f[0]

"""
#version récursive
def fibonacci(n):
    if n < 2:
        return n
    else:
        return fibonacci(n - 1) + fibonacci(n - 2)
"""


# tests

assert fibonacci(0) == 0
assert fibonacci(1) == 1
assert fibonacci(2) == 1
assert fibonacci(3) == 2
assert fibonacci(9) == 34
assert fibonacci(4) == 3
