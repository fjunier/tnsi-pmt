#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov 23 18:49:23 2023

@author: fjunier
"""
import doctest


def compte_uns2(tableau):
    """
    >>> compte_uns([0, 1, 1, 1])
    3
    >>> compte_uns([0, 0, 0, 1, 1])
    2
    >>> compte_uns([0] * 200)
    0
    >>> compte_uns([1] * 300)
    300
    >>> compte_uns([0] * 200 + [1] * 500)
    500
    >>> compte_uns([])
    0
    """

    def aux(tableau, binf, bsup):
        if bsup < binf:
            return 0
        if tableau[bsup] == 1 and tableau[binf] == 1:
            return bsup - binf + 1
        if tableau[bsup] < 1 or tableau[binf] > 1:
            return 0
        med = (binf + bsup) // 2
        return aux(tableau, binf, med) + aux(tableau, med + 1, bsup)

    return aux(tableau, 0, len(tableau) - 1)


def compte_uns(tableau):
    """
    >>> compte_uns([0, 1, 1, 1])
    3
    >>> compte_uns([0, 0, 0, 1, 1])
    2
    >>> compte_uns([0] * 200)
    0
    >>> compte_uns([1] * 300)
    300
    >>> compte_uns([0] * 200 + [1] * 500)
    500
    >>> compte_uns([])
    0
    """
    if len(tableau) > 0 and tableau[0] == 1:
        return len(tableau)
    a = 0
    b = len(tableau)
    while b - a > 1:
        m = (a + b) // 2
        if tableau[m] == 1:
            b = m
        else:
            a = m
    return len(tableau) - b


doctest.testmod(verbose=True)
