def est_bissextile(annee):
    if annee % 4 != 0:
        return False
    if annee % 100 != 0:
        return True
    if annee % 400 != 0:
        return False
    return True


# Tests
assert est_bissextile(2022) == False
assert est_bissextile(2020) == True
assert est_bissextile(2100) == False
assert est_bissextile(2400) == True
