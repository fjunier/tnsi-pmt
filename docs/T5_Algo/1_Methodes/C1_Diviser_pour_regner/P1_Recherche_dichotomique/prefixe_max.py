def prefixe_max2(c, d):
    """
    Renvoie le préfixe commun le plus long
    aux deux chaînes de caractères c et d
    """
    p = ""
    i = 0 
    while i < len(c) and i < len(d) and c[i] == d[i]:
        p = p + c[i]
        i = i + 1
    return p

def test_prefixe_max2():
    assert prefixe_max2('gact', 'gatt') == 'ga'
    assert prefixe_max2('gact', 'gacg') == 'gac'
    assert prefixe_max2('gact', 'gactg') == 'gact'
    assert prefixe_max2('gact', 'gact') == 'gact'
    assert prefixe_max2('gact', 'aact') == ''
    assert prefixe_max2('gact', '') == ''
    print("Tests réussis")

def prefixe_max_tab(t):
    """
    Renvoie le préfixe commun le plus long
    à toutes les chaînes de caractères du tableau t
    """
    if len(t) == 1:
        return t[0]
    if len(t) == 0:
        return ''
    m = len(t) // 2
    p1 = prefixe_max_tab(t[:m])
    p2 = prefixe_max_tab(t[m:])
    return prefixe_max2(p1, p2)

def test_prefixe_max_tab():
    assert prefixe_max_tab(['gatacca', 'gatacata', 'gacagagac', 'gaca']) == 'ga'
    assert prefixe_max_tab(['gttacca', 'gttagagac',  'ttaca']) == ''
    print("test réussi")
    
