---
title:  Synthèse du cours Diviser pour Régner 🎯
---


[version pdf](./synthese_dpr.pdf)


# Synthèse du cours Diviser pour Régner

## Recherche dichotomique

!!! note  "Point de cours 1 : recherche dichotomique dans un tableau trié"

     __Spécification du problème :__

    * On dispose d'un tableau `tab` d'éléments  de même type et compararables. De plus le tableau  est trié dans l'ordre croissant et les indices commencent à $0$.
    * On nous donne un élément `e` du même type que ceux du tableau.
    * On veut rechercher une occurrence de  `e` dans le tableau et renvoyer son indice. Si `e` n'est pas dans le tableau, on renverra $-1$. 
    
    
    L'algorithme de **recherche dichotomique** exploite l'ordre sur les éléments pour diviser au moins par deux la taille de la zone de recherche à chaque étape. On note `g` et `d` les indices délimitant à gauche et à droite la *zone de recherche*. On initialise `g` avec $0$ et `d` avec la longueur du tableau moins 1. Ensuite on répète les étapes suivantes jusqu'à ce que l'on trouve une occurrence de `e` ou que la *zone de recherche* soit vide (cas où `e` n'est pas dans le tableau).

    * __Diviser :__ On calcule l'indice du milieu de la zone de recherche `m = (g + d) // 2` et on se ramène à la résolution de trois sous-problèmes plus petits et similaires  :

    ![alt](images/dicho_ex1_schema.png)

    * __Résoudre :__ On résout directement le sous-problème 1 si `tab[m] = e` et sinon on résout récursivement l'un des deux autres sous-problèmes :
  
        * Si `e < tab[m]`, sous-problème 2 : l'élément `e` ne peut être que dans la première moitié de la zone de recherche correspondant aux indices dans l'intervalle `[g, m[`
        * Si `tab[m] < e`, sous-problème 3  : l'élément `e`  ne peut être que dans la seconde moitié de la zone de recherche correspondant aux indices dans l'intervalle `]m, d]`

    !!! tip "Implémentation itérative"

        ~~~python
            def recherche_dicho_iter(t, e):
                """
                Recherche dichotomique dans un tableau trié dans l'ordre croissant                          
                Paramètres :
                    t : tableau d'éléments  de même type et comparables, précondition  t trié dans l'ordre croissant
                    e : un élément du même type que ceux  dans tab
                Retour:
                    Si e dans tab renvoie l'index d'une occurrence sinon renvoie -1
                """
                g, d = 0, len(t) - 1
                while g <= d:
                    m = (g + d) // 2
                    # Sous-problème 1 :  occurrence de e en t[m]
                    if e == t[m]:
                        return m
                    # Sous-problème 2 :  occurrence de e en t[m]
                    #  on continue la recherche dans la première moitié [g, m [ = [g, m - 1]          
                    elif e < t[m]:
                        d = m - 1    
                    # Sous problème 3
                    # on continue la recherche dans la seconde moitié [m + 1, d]        
                    else:
                        g = m + 1
                return -1
        ~~~

    !!! tip "Implémentation récursive"

        ~~~python            
        def recherche_dicho_rec(t, e, g, d):
            m = (g + d) // 2
            # 1er cas de base : zone de recherche vide
            if g > d:
                return -1
            # Sous-problème 1
            # 2eme cas de base :  occurrence de e en t[m]
            if t[m] == e:
                return m 
            # Sous problème 2
            # on continue la recherche dans la première moitié [g, m [ = [g, m - 1]
            elif e < t[m]:
                return recherche_dicho_rec(t, e, g, m - 1)
            # Sous problème 3
            # on continue la recherche dans la seconde moitié [m + 1, d]
            else:
                return recherche_dicho_rec(t, e, m + 1, d)

        def recherche_dicho_rec_env(t, e):
            """Fonction enveloppe"""
            return recherche_dicho_rec(t, e, 0, len(t) - 1)
        ~~~

!!! note "Point de cours 2"

    La recherche dichotomique dans un tableau trié de taille $n$ est beaucoup plus efficace en nombre de comparaisons que la recherche séquentielle.

    |Algorithme de recherche dans un tableau trié|Complexité temporelle dans le pire des cas|Equivalent|
    |:---:|:---:|:---:|
    |Recherche dichotomique (Diviser pour Régner)|logarithmique $O(\log_{2}(n))$|Nombre de chiffres de $n$ en base 2|
    |Recherche séquentielle|linéaire $O(n)$|Valeur de $n$|

## Méthode _Diviser pour Régner_


!!! note "Point de cours 3"

    La méthode _Diviser Pour Régner_ [^DPR]  est une généralisation de la dichotomie.

    Un  algorithme de type _Diviser pour Régner_ est caractérisé par trois étapes :

    1. __Diviser :__ étant donné le problème à résoudre, on découpe l'entrée en deux ou  plusieurs *sous-problèmes similaires, plus petits et  indépendants*.
    2. __Résoudre :__  on résout tous les  sous-problèmes  :
          * soit directement si la solution est simple (_cas de base_)
          * soit en appelant l'algorithme sur le sous-problème (_appel récursif_)
    3. __Combiner :__  à partir des solutions des sous-problèmes on reconstitue une solution du problème initial [^combi].

    La réduction d'un problème à des sous-problèmes similaires  et plus petits, conduit naturellement à une programmation récursive.

    [^DPR]: _Divide and Conquer_ en anglais
    [^combi]: L'étape __Combiner__ est absente de la *recherche dichotomique* mais bien présente dans d'autres exemples vus en cours comme  la recherche de maximum _Diviser pour Régner_ ou le  *tri fusion*.

!!! abstract "Remarque"
    La méthode _Diviser pour Régner_ permet parfois d'améliorer la complexité en temps, comme pour  la recherche d'un élément dans un tableau trié,  mais ce n'est pas toujours le cas. Par exemple, l'algorithme _Diviser pour Régner_ de recherche du maximum dans un tableau d'entiers a une complexité linéaire, comme la recherche séquentielle.

    !!! exemple "Recherche de maximum DpR"

        ~~~python
        def maximum_dpr(tab):
            # Cas de base des appels récursifs : sous-problème de résolution directe
            if len(tab) == 1:
                return tab[0]
            # Diviser
            m = len(tab) // 2
            # Résoudre les 2 sous-problèmes
            m1 = maximum_dpr(tab[:m])
            m2 = maximum_dpr(tab[m:])
            # Combiner les solutions des sous-problèmes
            if m1 >= m2:
                return m1
            return m2
        ~~~

## Tri fusion

!!! note "Point de cours 3 : tri fusion"

    L'algorithme de **tri par fusion**  permet de trier un tableau `t` d'éléments comparables avec une méthode _Diviser pour Régner_ :

    1. __Diviser :__ on découpe le tableau en son milieu `m` et on se ramène à deux sous-problèmes similaires et plus petits :
        * trier le premier sous-tableau avec les éléments d'indice inférieur ou égal à `m`
        * trier le second sous-tableau avec les éléments d'indice supérieur à `m`
    2.  __Résoudre :__ on résout les deux sous-problèmes en appelant récursivement l'algorithme sur chaque sous-tableau et on obtient deux sous-tableaux triés  `t1` et  `t2`.
    3.  __Combiner :__ on fusionne les deux sous-tableaux triés `t1` et `t2` en un tableau  trié `t3` contenant les mêmes éléments que `t`.

    

    !!! example "exemple"   
         
        ![alt](images/tri_fusion_exo4_im1_mini.png)

        On a représenté par le schéma ci-dessus, la trace d'exécution  de l'algorithme  de __tri fusion__  qui trie dans l'ordre croissant le tableau d'entiers `[6, 3, 5, 2, 4, 0, 7 , 1]`.

    


    !!! tip "Implémentation récursive du tri fusion"
        ~~~python
        def tri_fusion(t):
            """Renvoie un tabbleau avec les mêmes éléments que t tableau d'entiers mais dans l'ordre croissant"""
            # cas de base:
            if len(t) <= 1:
                return t
            # diviser
            m = len(t) // 2
            # résoudre les sous-problèmes
            t1 = tri_fusion(t[:m])
            t2 = tri_fusion(t[m:])
            # combiner
            t3 = fusion(t1, t2)
            return t3
        ~~~


    !!! tip "Implémentation itérative  de  la fonction fusion"
        ~~~python
        def fusion(t1, t2):
            """
            Fusionne les tableaux d'entiers  t1 et t2 triés dans l'ordre croissant en un tableau  t3 trié dans l'ordre croissant  et constitué des mêmes    éléments  que t1 et t2  
            """
            n1 = len(t1)
            n2 = len(t2)
            t3 = []
            i1, i2 = 0, 0
            while i1 < n1 and i2 < n2:
                if t1[i1] <= t2[i2]:
                    t3.append(t1[i1])
                    i1 = i1 + 1
                else:
                    t3.append(t2[i2])
                    i2 = i2 + 1
            # ici un des deux tableaux t1 ou t2 est vide
            # cas il reste t1 non vide
            while i1 < n1:
                t3.append(t1[i1])
                i1 = i1 + 1
            # cas il reste t2 non vide
            while i2 < n2:
                t3.append(t2[i2])
                i2 = i2 + 1
            return t3
        ~~~

    !!! abstract "Remarque"
        Cette version n'effectue pas de tri en place en redistribuant les éléments dans le tableau initial, mais il est possible d'implémenter un _tri fusion_ en place avec la même complexité en temps en utilisant un tableau auxiliaire de stockage pour les fusions.


!!! note "Point de cours 4"
    La complexité  en temps du __tri fusion__ d'un tableau de taille $n$  est _linéarithmique_,  en $O(n \log_{2}(n))$. 
    Cette complexité est _optimale pour les tris par comparaison_ de deux éléments.  Le _tri fusion_ est plus efficace que les algorithmes de tri vus en classe de première qui sont de complexité _quadratique_, en $O(n^{2})$. Le _tri rapide_ est un autre algorithme _Diviser pour Régner_, très efficace. L'implémentation, plus délicate, sera vue en TP.

    |Algorithme de tri d'un tableau de taille $n$|Complexité dans le meilleur des cas|Complexité dans le cas moyen|Complexité dans le pire des cas|
    |:---:|:---:|:---:|:---:|
    |tri par sélection|$O(n^{2})$|$O(n^{2})$|$O(n^{2})$|
    |tri par insertion|$O(n)$ (tableau déjà trié)|$O(n^{2})$ (ordre inverse)|$O(n^{2})$|
    |tri par  bulles|$O(n^{2})$|$O(n^{2})$|$O(n^{2})$|
    |tri fusion|$O(n\log_{2}(n))$|$O(n\log_{2}(n))$|$O(n\log_{2}(n))$|
    |tri rapide|$O(n\log_{2}(n))$|$O(n\log_{2}(n))$|$O(n^{2})$ (tableau déjà trié)|

    ![alt](images/bigo-mini.png)

    > _Source :_ <https://www.hackerearth.com/practice/notes/sorting-and-searching-algorithms-time-complexities-cheat-sheet/>
