def rendu_monnaie_dyna(montant, pieces):
    """Renvoie le nombre minimal de pièces pour rendre la monnaie
    sur montant avec le système monétaire pieces qui contient une pièce de 1"""
    memo = [0 for _ in range(montant + 1)]
    for m in range(1, montant + 1):
        memo[m] = m # m pièces de 1, pire des cas
        for p in pieces:
            if p <= m:
                # à compléter
                ...
    return memo[montant]


def test_rendu_monnaie_dyna():
    assert rendu_monnaie_dyna(10, [1, 2]) == 5
    assert rendu_monnaie_dyna(8, [1, 4, 6]) == 2
    systeme_euro = [1, 2, 5, 10, 20, 50, 100, 200, 500]
    assert rendu_monnaie_dyna(49, systeme_euro) == 5
    assert rendu_monnaie_dyna(76, systeme_euro) == 4
    print("tests réussis")
    
test_rendu_monnaie_dyna()