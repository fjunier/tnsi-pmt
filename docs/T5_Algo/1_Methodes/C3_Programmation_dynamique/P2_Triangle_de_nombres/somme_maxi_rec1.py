def s(i, j):
    """Renvoie la somme maximale dans le sous-triangle de sommet
    en ligne i et colonne j dans un triangle de nombres t
    qui est une liste de listes défini comme variable globale"""
    if i == len(t) - 1:
        return ...
    return ...

t = [[3], [40, 38], [34, 31, 33], [3, 4, 22, 25], [42, 24, 41, 38, 5]]
assert s(0, 0) == 137